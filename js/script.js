var tempValues = new Array();
var city = "Athlone";
var current_Date_Data = [];

// Fetching API Data
var getXmlObj = function(city) {
  var request = new XMLHttpRequest();
  request.onreadystatechange = function() {
    if (this.readyState == 4 && this.status == 200) {
      getData(this);
    }
  };
  request.open(
    "GET",
    "http://api.openweathermap.org/data/2.5/forecast?q=" +
      city +
      "&APPID=bd180698f2fb9f079df486671df34281&mode=xml&units=metric",
    true
  );

  request.send();
};

// Storing the XML data in JS Object
var getData = function(xml) {
  var data = xml.responseXML;
  var x = data.getElementsByTagName("weatherdata");
  var x1 = x[0]
    .getElementsByTagName("forecast")[0]
    .getElementsByTagName("time");

  var date = [];
  for (i = 1; i < x1.length; i++) {
    var temp_date = x1[i].getAttribute("from").split("T");
    if (date[0] != temp_date[0]) {
      date = temp_date;
      var currDateWeather = {
        symbol: x1[i].getElementsByTagName("symbol")[0].getAttribute("name"),
        symbol_id: x1[i]
          .getElementsByTagName("symbol")[0]
          .getAttribute("number"),
        windDirection: x1[i]
          .getElementsByTagName("windDirection")[0]
          .getAttribute("code"),
        windSpeed: x1[i]
          .getElementsByTagName("windSpeed")[0]
          .getAttribute("mps"),
        temperature: {
          unit: x1[i]
            .getElementsByTagName("temperature")[0]
            .getAttribute("unit"),
          value: x1[i]
            .getElementsByTagName("temperature")[0]
            .getAttribute("value"),
          min: x1[i].getElementsByTagName("temperature")[0].getAttribute("min"),
          max: x1[i].getElementsByTagName("temperature")[0].getAttribute("max")
        },
        pressure: x1[i]
          .getElementsByTagName("pressure")[0]
          .getAttribute("value"),
        humidity: x1[i]
          .getElementsByTagName("humidity")[0]
          .getAttribute("value"),
        clouds: x1[i].getElementsByTagName("clouds")[0].getAttribute("value")
      };
      tempValues.push(currDateWeather);
    }
  }
  printData();
};

// Setting application to default values
window.onload = function() {
  document.getElementById("search").value = "";
  getXmlObj("Athlone");
};

// Get data of City present in text box
var getWeather = () => {
  city = document.getElementById("search").value;
  tempValues = [];
  getXmlObj(city.toLowerCase());
};

// Print data in HTML page
var printData = () => {
  for (i = 0; i < 6; i++) {
    d = new Date();
    d.setDate(d.getDate() + i);
    var current_day_object = {
      date: d.getDate(),
      month: getMonth(d),
      day: getWeekDay(d)
    };
    current_Date_Data.push(current_day_object);
  }

  document.getElementById("city_name").innerText =
    city.charAt(0).toUpperCase() + city.substring(1).toLowerCase();
  document.getElementById("temp").innerText =
    tempValues[0].temperature.value + " °C";
  document.getElementById("weather").innerText =
    tempValues[0].symbol.charAt(0).toUpperCase() +
    tempValues[0].symbol.substring(1);
  document.getElementById("curr_day").innerText =
    current_Date_Data[0].day +
    ", " +
    current_Date_Data[0].date +
    " " +
    current_Date_Data[0].month;
  document.getElementById("humidity").innerText = tempValues[0].humidity + " %";
  document.getElementById("wind_speed").innerText =
    parseInt(tempValues[0].windSpeed) * 3.6 + " Km/h";
  document.getElementById("wind_dir").innerText = tempValues[0].windDirection;
  document.getElementById("pressure").innerText =
    tempValues[0].pressure + " hPa";

  d = new Date();
  if (tempValues[0].symbol_id == 800) {
    if (d.getHours() > 18) {
      document
        .getElementById("gif_weather")
        .setAttribute(
          "style",
          "background-image: url(images/gif/night.gif); background-repeat: no-repeat; background-size: cover;"
        );
      document
        .getElementById("icon_weather")
        .setAttribute("src", "images/weather/light/night-sunny.png");
    } else {
      document
        .getElementById("gif_weather")
        .setAttribute(
          "style",
          "background-image: url(images/gif/sunny.gif); background-repeat: no-repeat; background-size: cover;"
        );
      document
        .getElementById("icon_weather")
        .setAttribute("src", "images/weather/light/sunny.png");
    }
  } else if (thunderstorm_Check(tempValues[0].symbol_id) === true) {
    document
      .getElementById("gif_weather")
      .setAttribute(
        "style",
        "background-image: url(images/gif/thunderstorm.gif); background-repeat: no-repeat; background-size: cover;"
      );
    document
      .getElementById("icon_weather")
      .setAttribute("src", "images/weather/light/thunderstorm.png");
  } else if (drizzle_Check(tempValues[0].symbol_id) === true) {
    document
      .getElementById("gif_weather")
      .setAttribute(
        "style",
        "background-image: url(images/gif/mist.gif); background-repeat: no-repeat; background-size: cover;"
      );
    document
      .getElementById("icon_weather")
      .setAttribute("src", "images/weather/light/mostly-cloud.png");
  } else if (rain_Check(tempValues[0].symbol_id) === true) {
    document
      .getElementById("gif_weather")
      .setAttribute(
        "style",
        "background-image: url(images/gif/rain.gif); background-repeat: no-repeat; background-size: cover;"
      );
    document
      .getElementById("icon_weather")
      .setAttribute("src", "images/weather/light/rain.png");
  } else if (snow_Check(tempValues[0].symbol_id) === true) {
    document
      .getElementById("gif_weather")
      .setAttribute(
        "style",
        "background-image: url(images/gif/snow.gif); background-repeat: no-repeat; background-size: cover;"
      );
    document
      .getElementById("icon_weather")
      .setAttribute("src", "images/weather/light/snow.png");
  } else if (clouds_Check(tempValues[0].symbol_id) === true) {
    document
      .getElementById("gif_weather")
      .setAttribute(
        "style",
        "background-image: url(images/gif/clouds.gif); background-repeat: no-repeat; background-size: cover;"
      );
    document
      .getElementById("icon_weather")
      .setAttribute("src", "images/weather/light/mostly-cloud.png");
  }
  console.log(tempValues);
  createChart();
};
// Following Functions are to check their relevent values
var getWeekDay = d => {
  var weekday = new Array(7);
  weekday[0] = "Sunday";
  weekday[1] = "Monday";
  weekday[2] = "Tuesday";
  weekday[3] = "Wednesday";
  weekday[4] = "Thursday";
  weekday[5] = "Friday";
  weekday[6] = "Saturday";
  return weekday[d.getDay()];
};

var getMonth = d => {
  var monthNames = [
    "January",
    "February",
    "March",
    "April",
    "May",
    "June",
    "July",
    "August",
    "September",
    "October",
    "November",
    "December"
  ];

  return monthNames[d.getMonth()];
};

var thunderstorm_Check = val => {
  var id = [200, 201, 202, 210, 211, 212, 221, 230, 231, 232];
  for (i = 0; i < id.length; i++) {
    if (id[i] == val) {
      return true;
    }
  }
  return false;
};

var drizzle_Check = val => {
  var id = [300, 301, 302, 310, 311, 312, 313, 314, 321];
  for (i = 0; i < id.length; i++) {
    if (id[i] == val) {
      return true;
    }
  }
  return false;
};

var rain_Check = val => {
  var id = [500, 501, 502, 503, 504, 511, 520, 521, 522, 531];
  for (i = 0; i < id.length; i++) {
    if (id[i] == val) {
      return true;
    }
  }
  return false;
};

var snow_Check = val => {
  id = [600, 601, 602, 611, 612, 613, 615, 616, 620, 621, 622];
  for (i = 0; i < id.length; i++) {
    if (id[i] == val) {
      return true;
    }
  }
  return false;
};

var clouds_Check = function(val) {
  id = [801, 802, 803, 804];
  for (i = 0; i < id.length; i++) {
    if (id[i] == val) {
      return true;
    }
  }
  return false;
};

// This function creates and represents char in the main HTML file
var createChart = function() {
  var MONTHS = [];
  for (i = 1; i < current_Date_Data.length; i++) {
    MONTHS.push(current_Date_Data[i].day);
  }
  var DATES = [];
  for (i = 1; i < current_Date_Data.length; i++) {
    DATES.push(
      current_Date_Data[i].date + " " + current_Date_Data[i].day.substring(0, 3)
    );
  }

  var MIN_WEA = [];
  for (i = 0; i < tempValues.length; i++) {
    MIN_WEA.push(tempValues[i].temperature.min);
  }

  console.log(MIN_WEA);

  var MAX_WEA = [];
  for (i = 0; i < tempValues.length; i++) {
    MAX_WEA.push(tempValues[i].temperature.max);
  }

  console.log(MAX_WEA);

  var config = {
    type: "line",
    data: {
      labels: DATES,

      datasets: [
        {
          label: "Maximum Temprature",
          backgroundColor: "#ff0000",
          borderColor: "#ff0000",
          data: MAX_WEA,
          fill: false
        },
        {
          label: "Minimum Temprature",
          fill: false,
          backgroundColor: "#33ccff",
          borderColor: "#33ccff",
          data: MIN_WEA
        }
      ]
    },
    options: {
      responsive: true,
      title: {
        display: false,
        text: ""
      },
      tooltips: {
        mode: "index",
        intersect: false
      },
      hover: {
        mode: "nearest",
        intersect: true
      },
      scales: {
        xAxes: [
          {
            display: true,
            scaleLabel: {
              display: false,
              labelString: "Days"
            }
          }
        ],
        yAxes: [
          {
            display: false,
            scaleLabel: {
              display: true,
              labelString: "Value"
            }
          }
        ]
      }
    }
  };
  var ctx = document.getElementById("canvas").getContext("2d");
  window.myLine = new Chart(ctx, config);
};
